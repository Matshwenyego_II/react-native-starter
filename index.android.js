import React from 'react';
import {
  AppRegistry,
} from 'react-native';
import App from './app/index'; 


AppRegistry.registerComponent('image_picker', () => App);