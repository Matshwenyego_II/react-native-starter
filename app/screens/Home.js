import React, {Component} from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Image, 
    TextInput
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

var ImagePicker = require('react-native-image-picker');


export default class Home extends Component{
    

    render(){

        return(
            <View style={{flex: 1}}>
                
                <TouchableOpacity style={{alignSelf: 'center'}}
                onPress={this.attachMedia.bind(this)}>
                <Text><Icon name="file-upload" size={14} /> Attach Media</Text>
                    {/*<Image source={this.state.avatarSource} />*/}
                </TouchableOpacity>

            </View>
        );
    }

    attachMedia(){
        this.setState({ uploadURL: '' })
        
          ImagePicker.showImagePicker({title: 'Select Image'}, (response)=>{
            console.log(response);
            if (response.didCancel) {
              this.setState({
                uploadURL: null
              });
            }
            else if (response.error) {
              this.setState({
                uploadURL: null
              });
            }
            else if (response.customButton) {
              this.setState({
                uploadURL: null
              });
            }else{
            const image = {
              uri: response.uri,
              type: response.type,
              name: response.fileName
            }
            this.setState({
              uploadURL: response.uri
            });
            this.state.post.append('image', image);
          }
          });
    }
}
